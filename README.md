# cympire-visualization
## by Olivier Guyot

## Error Cross domain with api
## I had a cross-domain error from your server. So I used the data in the project. You will still see my API call on the line (398) Home.vue

##Access to XMLHttpRequest at 'https://i3gy725noe.execute-api.us-east-1.amazonaws.com/default/VisualizatorApi' from origin 'http://localhost:8080' has been blocked by CORS policy: Response to preflight request doesn't pass access control check: No 'Access-Control-Allow-Origin' header is present on the requested resource.



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
